# README #

Mars Rover Challenge

Rovers have been sent to Mars to survey the terrain and you have been charged with creating their navigation system.

These are the specifications you have been given:

* Mars�s surface has been divided into zones and each zone can be modelled as a two-dimensional cartesian grid. The zones have been very carefully surveyed ahead of time and are deemed safe for exploration within the zone�s bounds, as represented by a single cartesian coordinate. E.g: (5, 5)
* The rover understands the cardinal points and can face either East (E), West (W), North (N) or South (S)
* The rover understands three commands:
* * M - Move one space forward in the direction it is facing
* * R - rotate 90 degrees to the right
* * L - rotate 90 degrees to the left

Due to the transmission delay in communicating with the rover on Mars, you are only able to send the rover a list of commands. These commands will be executed by the rover and its resulting location sent back to HQ. This is an example of the list of commands sent to the rover:

* 8 8
* 1 2 E
* MMLMRMMRRMML

This is how the rover will interpret the commands:

* The first line describes how big the current zone is. This zone�s boundary is at the Cartesian coordinate of 8,8 and the zone comprises 64 blocks.
* The second line describes the rover�s staring location and orientation. This rover would start at position 1 on the horizontal axis, position 2 on the vertical axis and would be facing East (E).
* The third line is the list of commands (movements and rotations) to be executed by the rover.

As a result of following these commands, a rover staring at 1 2 E in this zone would land up at 3 3 S.

### Objective ###

* Develop a program that can import a text file with commands sent to the Mars Rover and report on Rover's End Location after commands have been interpreted.

### How do I get set up? ###

* Download both the index.php and process_file.php scripts, install and run on a web server.
* Download the rover1.txt file

The rover1.txt file can be edited with different commands and options as described above.

### Developer Notes ###

I've used Bootstrap, jQuery, PHP and Javascript to develop this script, however the script can be modified to just run in HTML and JavaScript.
I've developed both a functional and visual script, and added both the option to upload a Rover Command file, or enter the Rover commands manually via a form input on screen.
The script will "draw" the Rover's grid and show the Rover's starting Position, Tracks covered and Ending position in a visual way.
As far as the Command Interpretation is concerned, logic has been applied so that whatever direction the Rover is moving, if the commands are out of the grid bounds, the Movement command would be ignored.

* The biggest challenge of this program was the CSS styling and visual representation of the Rover, although it was not part of the request.


### Who do I talk to? ###

* @iamfr4n - Francois Jonker - fjonker@gmail.com